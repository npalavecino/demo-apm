import { Controller, Get, Req, HttpCode } from '@nestjs/common';
import { Request } from '@nestjs/common';
import Axios from 'axios';

function allocateMemory(size) {
  const numbers = size / 8;
  const arr = [];
  arr.length = numbers;
  for (let i = 0; i < numbers; i++) {
    arr[i] = i;
  }
  return arr;
}

@Controller()
export class AppController {

  @Get('/:prefix/get-error')
  @HttpCode(500)
  getHello(): string {
    return 'Upss la cagaste!';
  }

  @Get('/:prefix/hello-world')
  getError(): string {
    return 'Hola a todos!';
  }

  @Get('/:prefix/kill')
  getKill() {
    const memoryLeakAllocations = [];
    const field = "heapUsed";
    const allocationStep = 10000 * 1024; // 10MB
    
    const TIME_INTERVAL_IN_MSEC = 10;
    
    setInterval(() => {
      const allocation = allocateMemory(allocationStep);
    
      memoryLeakAllocations.push(allocation);
    
      const mu = process.memoryUsage();
      // # bytes / KB / MB / GB
      const gbNow = mu[field] / 1024 / 1024 / 1024;
      const gbRounded = Math.round(gbNow * 100) / 100;
    
      console.log(`Heap allocated ${gbRounded} GB`);
    }, TIME_INTERVAL_IN_MSEC);
  }

  @Get('healthy')
  getHealthy(): { status: string } {
    return {
      status: 'ok',
    };
  }

  @Get('/parent/kill-child')
  async killChild(
    @Req() request: Request
  ): Promise<{ status: string }> {
    const url = request.headers['host'];
    console.log(url);
    await Axios.get('http://' + url + '/child/kill');

    return {
      status: 'ok',
    };
  }
}
