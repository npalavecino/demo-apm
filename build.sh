#!/bin/bash
echo 'Creating .env'
IFS=$'\n'
for line in $(cat .env-template)
do
	line="${line/=/}"
	branch="$(echo $CI_COMMIT_BRANCH | tr [a-z] [A-Z])"
	secret=$branch"-"$line
	value="$(google-cloud-sdk/bin/gcloud secrets versions access latest --secret=$secret --project=$GCLOUD_PROJECT_ID)"
	echo $line"="$value >> .env
done
echo '.env finished'