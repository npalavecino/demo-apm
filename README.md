# DEMO APM
## _Como un APM te puede salvar un fin de semana_

## Crear ambiente Prueba


```sh
- Crear proyecto en gcp
- Crear service account con permisos de editor para prueba.
- Crear keys --> tipo json
- Cargar variables en CI/CD
- Crear cluster k8s GKE 
   - Kubernetes Engine API --> ENABLE 
- gcloud compute networks create vpc-k8s
- gcloud container clusters create demo-cluster --num-nodes 3 --network vpc-k8s --zone us-central1-a  --scopes=storage-rw,compute-ro
- GKE Autopilot --> configure --> Cargar nombre y aplicar configuraciones por defecto
- Configurar cluster ---> :hourglass_flowing_sand:

- ✨Magic ✨
```




## Variebles CI

Configurar variables en    https://gitlab.com/<USER>/demo-apm/-/settings/ci_cd

| VARIABLE | VALUE |
| ------ | ------ |
| APIKEY_DD | Base_64*** |
| GCLOUD_CLUSTER_NAME| demo-cluster |
| GCLOUD_PROJECT_ID| *** |
| GCLOUD_SERVICE_KEY | *** |
| K8S_NAME| demo-cluster |
